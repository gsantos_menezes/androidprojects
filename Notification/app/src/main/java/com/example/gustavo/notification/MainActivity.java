package com.example.gustavo.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button notificacao = (Button) findViewById(R.id.button);
        notificacao.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                NotificationChannel channel = new NotificationChannel("default","channel", NotificationManager.IMPORTANCE_DEFAULT);
                NotificationManager notificationManager = (NotificationManager) v.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                channel.setDescription("Channel de notificação");
                notificationManager.createNotificationChannel(channel);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(v.getContext(), "default");
                notificationBuilder.setSmallIcon(R.drawable.notification_icon);
                notificationBuilder.setContentTitle("Exemplo");
                notificationBuilder.setContentText("Exemplo de notificação");
                notificationManager.notify(1, notificationBuilder.build());
            }
        });

    }
}
